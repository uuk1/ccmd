extern "C" {
#include <string.h>	
#include "radix.h"
}
#include "router.hh"

#include <iostream>

using namespace std;


// Dummy implementation of an IP router

// Given an incoming Internet datagram, the router decides
// (1) which interface to send it out on, and
// (2) what next hop address to send it to.

// For Lab 6, please replace with a real implementation that passes the
// automated checks run by `make check_lab6`.

// You will need to add private members to the class declaration in `router.hh`

template <typename... Targs>
void DUMMY_CODE(Targs &&... /* unused */) {}
static void pfr_prepare_network(union sockaddr_union *sa, int af, int net)
{
        int i;

        memset(sa, 0, sizeof(*sa));
        if (af == AF_INET) {
                sa->sin.sin_len = sizeof(sa->sin);
		sa->sin.sin_family = AF_INET;
                sa->sin.sin_addr.s_addr = net ? htonl(0xffffffff << (32 - net)) : 0;
        } else if (af == AF_INET6) {
                sa->sin6.sin6_len = sizeof(sa->sin6);
                sa->sin6.sin6_family = AF_INET6;
                for (i = 0; i < 4; i++) {
                        if (net <= 32) {
                                sa->sin6.sin6_addr.s6_addr32[i] =
 	                            net ? htonl(0xffffffff << (32 - net)) : 0;
                                break;
                        }
	                sa->sin6.sin6_addr.s6_addr32[i] = 0xFFFFFFFF;
			net -= 32;
                }
        }
}
std::vector<rt_entry *> _route_list{};
std::vector<struct radix_mask *> rm_list{}; 
//! \param[in] route_prefix The "up-to-32-bit" IPv4 address prefix to match the datagram's destination address against
//! \param[in] prefix_length For this route to be applicable, how many high-order (most-significant) bits of the route_prefix will need to match the corresponding bits of the datagram's destination address?
//! \param[in] next_hop The IP address of the next hop. Will be empty if the network is directly attached to the router (in which case, the next hop address should be the datagram's final destination).
//! \param[in] interface_num The index of the interface to send the datagram out on.
void Router::add_route(const uint32_t route_prefix,
                       const uint8_t prefix_length,
                       const optional<Address> next_hop,
                       const size_t interface_num) {
    rt_entry *rt = new rt_entry;
    union sockaddr_union mask;
    unsigned int net;
    rt->ip = route_prefix;
    net = prefix_length;
    rt->net = net;
    rt->sin.sin_len = sizeof(struct sockaddr_in_bsd);
    rt->sin.sin_family = AF_INET;
    rt->sin.sin_addr.s_addr = ntohl(rt->ip);
    rt->routeitem.route_prefix = route_prefix;
    rt->routeitem.prefix_length = prefix_length;
    rt->routeitem.next_hop = next_hop;
    rt->routeitem.interface_num = interface_num;
    
    if (net < 32) {
	    pfr_prepare_network(&mask, AF_INET, net);
	    rn_addroute(&rt->sin, &mask, &rt_table_local, rt->rt_node);
    } else {
	    rn_addroute(&rt->sin, NULL, &rt_table_local, rt->rt_node);
    }
    _route_list.push_back(rt);
}

//! \param[in] dgram The datagram to be routed
void Router::route_one_datagram(InternetDatagram &dgram) {
    RouteItem item;
    uint32_t dst_ip = dgram.header().dst;
    struct sockaddr_in_bsd sin{0,0,0,0,0,0,0,0,0,0,0,0};
    sin.sin_len = sizeof (struct sockaddr_in_bsd);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ntohl(dst_ip);
    radix_node *rn;
    rt_entry *rt;
    rn = rn_match(&sin, &rt_table_local);
    rt = reinterpret_cast<struct rt_entry_s *>(rn);

    if (!rt) {
        return;
    }
    if (dgram.header().ttl <= 1) {
        return;
    }

    dgram.header().ttl--;
    if (rt->routeitem.next_hop.has_value()) {
        _interfaces[rt->routeitem.interface_num].send_datagram(dgram, rt->routeitem.next_hop.value());
    } else {  // if not have next_hop, the next_hop is dgram's destination hop
        _interfaces[rt->routeitem.interface_num].send_datagram(dgram, Address::from_ipv4_numeric(dgram.header().dst));
    }
}

void Router::route() {
    // Go through all the interfaces, and route every incoming datagram to its proper outgoing interface.
    for (auto &interface : _interfaces) {
        auto &queue = interface.datagrams_out();
        while (not queue.empty()) {
            route_one_datagram(queue.front());
            queue.pop();
        }
    }
}

bool Router::prefix_equal(uint32_t ip1, uint32_t ip2, uint8_t len) {
    // special judge right when shift 32 bit
    uint32_t offset = (len == 0) ? 0 : 0xffffffff << (32 - len);
    printf("ip cmp: %x %x, offset: %x\n", ip1 & offset, ip2 & offset, offset);
    return (ip1 & offset) == (ip2 & offset);
}

Router::~Router() {
	for (size_t i = 0; i < _route_list.size(); i++) {
		delete _route_list[i];
	}
	for (size_t i = 0; i < rm_list.size(); i++) {
		free(rm_list[i]);
	}
}
extern "C" {
	void getmask(struct radix_mask * rm);
}
void getmask(struct radix_mask * rm) {
	rm_list.push_back(rm); 	
}
	
